data "aws_ami" "centos" {
  most_recent = true
  filter {
    name   = "name"
    values = ["CentOS Linux 7 x86_64*"]
  }
  filter {
    name   = "virtualization-type"
    values = ["hvm"]
  }
  filter {
    name   = "product-code"
    values = ["aw0evgkw8e5c1q413zgy5pjce"]
  }
  owners   = ["679593333241"] # aws
}

resource "aws_instance" "public-node" {
  count         = "${length(data.aws_availability_zones.available_az.names)}"
  ami           = "${data.aws_ami.centos.id}"
  instance_type = "${var.instance_type}"
  subnet_id     = "${element(aws_subnet.public_test.*.id, count.index)}"
  key_name      = "${var.key-pair}"
  associate_public_ip_address = "true"
  vpc_security_group_ids = ["${aws_security_group.test_public_sg.id}"]
  tags = {
    Name  = "public-node${(count.index)}"
    Owner = "test"
  }
}


resource "aws_instance" "private-node" {
  count         = "${length(data.aws_availability_zones.available_az.names)}"
  ami           = "${data.aws_ami.centos.id}"
  instance_type = "${var.instance_type}"
  #subnet_id    = "${aws_subnet.private_test.id}"
  subnet_id     = "${element(aws_subnet.private_test.*.id, count.index)}"
  key_name      = "${var.key-pair}"
  root_block_device {
    volume_size = 8
    volume_type = "gp2"
    delete_on_termination = true
  }
  vpc_security_group_ids = ["${aws_security_group.test_private_sg.id}"]
  tags = {
    Name  = "private-node${(count.index)}"
    Owner = "test"
  }
}

resource "aws_ebs_volume" "private-vol" {
  count             = "${length(data.aws_availability_zones.available_az.names)}"
  availability_zone = "${element(data.aws_availability_zones.available_az.names, count.index)}"
  size              = 10
  tags = {
    Name = "Test"
    Owner = "test"
  }
}

resource "aws_volume_attachment" "private_ebs_attach" {
  count = "${length(data.aws_availability_zones.available_az.names)}"
  device_name = "/dev/sdh"
  volume_id   = "${element(aws_ebs_volume.private-vol.*.id, count.index)}"
  instance_id = "${element(aws_instance.private-node.*.id, count.index)}"
}
