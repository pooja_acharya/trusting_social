resource "aws_vpc" "test-dev" {
  cidr_block = "${var.vpc["vpc_cidr"]}"
  tags = {
    Name  = "test-dev"
    Owner = "test"
  }
}

data "aws_availability_zones" "available_az" {
  state = "available"
}


resource "aws_subnet" "private_test" {
  count             = "${length(data.aws_availability_zones.available_az.names)}"
  vpc_id            = "${aws_vpc.test-dev.id}"
  availability_zone = "${element(data.aws_availability_zones.available_az.names, count.index)}"
  cidr_block        = "${cidrsubnet("10.0.10.0/24", 4, count.index)}"
  map_public_ip_on_launch = "false"
  tags = {
    Name  = "private-test-${element(data.aws_availability_zones.available_az.names, count.index)}"
    Owner = "test"
  }
}

resource "aws_subnet" "public_test" {
  count             = "${length(data.aws_availability_zones.available_az.names)}"
  availability_zone = "${element(data.aws_availability_zones.available_az.names, count.index)}"
  vpc_id            = "${aws_vpc.test-dev.id}"
  cidr_block        = "${cidrsubnet("10.0.1.0/24", 4, count.index)}"
  map_public_ip_on_launch = "true"
  tags = {
    Name  = "public-test-${element(data.aws_availability_zones.available_az.names, count.index)}"
    Owner = "test"
  }
}

resource "aws_internet_gateway" "test-igw" {
  vpc_id  = "${aws_vpc.test-dev.id}"
  tags = {
    Name  = "test-igw"
    Owner = "test"
  }
}

resource "aws_route_table" "test-public-rt" {
  vpc_id  = "${aws_vpc.test-dev.id}"
  route {
      cidr_block = "0.0.0.0/0"
      gateway_id = "${aws_internet_gateway.test-igw.id}"
    }
  tags = {
    Name  = "test-public-rt"
    Owner = "test"
  }
}

resource "aws_route_table_association" "test-public-rta" {
    count          = "${length(data.aws_availability_zones.available_az.names)}"
    subnet_id      = "${element(aws_subnet.public_test.*.id, count.index)}"
    route_table_id = "${aws_route_table.test-public-rt.id}"
}

resource "aws_eip" "nat-eip" {
vpc      = true
}

resource "aws_nat_gateway" "test-ngw" {

  subnet_id     = "${element(aws_subnet.public_test.*.id, count.index)}"
  allocation_id = "${aws_eip.nat-eip.id}"
  tags = {
    Name  = "test-ngw"
    Owner = "test"
  }
}

resource "aws_route_table" "test-private-rt" {
  vpc_id         = "${aws_vpc.test-dev.id}"
  route {
      cidr_block = "0.0.0.0/0"
      gateway_id = "${aws_nat_gateway.test-ngw.id}"
    }
  tags = {
    Name  = "test-private-rt"
    Owner = "test"
  }
}

resource "aws_route_table_association" "test-private-rta" {
    count           = "${length(data.aws_availability_zones.available_az.names)}"
    subnet_id       = "${element(aws_subnet.private_test.*.id, count.index)}"
    route_table_id  = "${aws_route_table.test-private-rt.id}"
}

