#Insert your AWS credentials or attach IAM Role

variable "aws_access_key" {
  type    = "string"
  default = ""
}

variable "aws_secret_key" {
  type    = "string"
  default = ""
}

variable "region" {
  type    = "string"
  default = "eu-west-1"
}

variable "vpc" {
  type    = "map"
  default = {
    vpc_cidr = "10.0.0.0/16"
  }
}

#Insert your network IP to be whitelisted for SSH access on public machines

variable "my_network" {
  type    = "map"
  default = {
    my_cidr =  ["155.63.243.5/32"]
}
}

variable "key-pair"{
  type    = "string"
  default = "test-keypair"
}

variable "instance_type"{
  type    = "string"
  default = "t2.micro"
}

variable "certificate_arn"{
  type    = "string"
  default = "<<account-no>>:server-certificate/<<certificate-name>>"
}

