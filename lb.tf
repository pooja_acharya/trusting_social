resource "aws_lb" "test-nlb" {

  name               = "test-network-lb"
  internal           = false
  load_balancer_type = "network"
   subnets            = ["${aws_subnet.public_test.*.id}"]
  
  enable_cross_zone_load_balancing = true
  tags = {
    Name = "test-lb"
    Owner = "test"
  }
}

resource "aws_lb_target_group" "test-tg-80" {
  name     = "test-lb-tg-80"
  port     = 80
  protocol = "TCP"
  vpc_id   = "${aws_vpc.test-dev.id}"
  stickiness = []
}

resource "aws_lb_target_group" "test-tg-443" {
  name     = "test-lb-tg-443"
  port     = 443
  protocol = "TLS"
  vpc_id   = "${aws_vpc.test-dev.id}"
  stickiness = []
}

resource "aws_lb_target_group_attachment" "lb-tg-80" {
  count = "${length(data.aws_availability_zones.available_az.names)}"
  target_group_arn = "${aws_lb_target_group.test-tg-80.arn}"
  target_id        = "${element(aws_instance.private-node.*.id, count.index)}"
  port             = 80
}

resource "aws_lb_target_group_attachment" "lb-tg-443" {
  count = "${length(data.aws_availability_zones.available_az.names)}"
  target_group_arn = "${aws_lb_target_group.test-tg-443.arn}"
  target_id        = "${element(aws_instance.private-node.*.id, count.index)}"
  port             = 443
}

resource "aws_lb_listener" "listener-443" {
  load_balancer_arn = "${aws_lb.test-nlb.arn}"
  port              = "443"
  protocol          = "TLS"
  ssl_policy        = "ELBSecurityPolicy-2016-08"
  certificate_arn   = "${var.certificate_arn}"
  default_action {
    type             = "forward"
    target_group_arn = "${aws_lb_target_group.test-tg-443.arn}"
  }
}

resource "aws_lb_listener" "listener-80" {
  load_balancer_arn = "${aws_lb.test-nlb.arn}"
  port              = "80"
  protocol          = "TCP"
  default_action {
    type             = "forward"
    target_group_arn = "${aws_lb_target_group.test-tg-80.arn}"
  }
}
